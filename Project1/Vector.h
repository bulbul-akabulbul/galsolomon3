#ifndef VECTOR_H
#define VECTOR_H
#include <iostream>

#define MINIMUM 2
#define EMPTY -9999

class Vector {
private:
	//Fields
	int* _elements;
	int _capacity; //Total memory allocated
	int _size; //Size of vector to access
	int _resizeFactor; // how many cells to add when need to reallocate
public:

	//A

	// Allocates a new vector with size N and resizeFactor N
	Vector(int n);
	~Vector();
	int size() const;//return size of vector
	int capacity() const;//return capacity of vector
	int resizeFactor() const; //return vector's resizeFactor
	bool empty() const; //returns true if size = 0

	//B
	//Modifiers

	// Adds a value to the end of the vector.
	void push_back(const int& val);//adds element at the end of the vector
	// Removes the last element and returns it.
	int pop_back();//removes and returns the last element of the vector
	//Makes sure the vector has capacity of at least n.
	// Allocates more memory if needed.
	void reserve(int n);//change the capacity
	// Resizes the vector to a new size.
	// May delete values.
	void resize(int n);//change _size to n, unless n is greater than the vector's capacity
	// Assigns val in every element
	void assign(int val);//assigns val to all elemnts
	// Resizes the vector to a new size, and assigns val in every new element
	// May delete values.
	void resize(int n, const int& val);//same as above, if new elements added their value is val

	//C
	//The big three (d'tor is above)

	// Copy constructor
	Vector(const Vector& other);
	// Copies only the elements
	Vector& operator=(const Vector& other);

	//D
	//Element Access

	// Allows for direct access to an element (READ ONLY)
	int& operator[](int n) const;//n'th element

	// Bonuses

	// Sums elements in the same index
	Vector operator+(const Vector& other) const;
	// Subtracts elements in the same index
	Vector operator-(const Vector& other) const;
	// Subtracts the right from the left
	void operator-=(Vector& other);
	// Adds right to the left
	void operator+=(Vector& other);
	// Prints info about the vector
	friend std::ostream& operator<<(std::ostream& os, const Vector& v);

	// My bonuses

	// Multiplies elements in the same index
	Vector operator*(const Vector& other) const;
	// Divides elements in the same index
	Vector operator/(const Vector& other) const;
	// Adds every element in the vector with the int
	Vector operator+(const int other) const;


};

#endif