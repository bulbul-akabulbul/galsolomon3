#include "Vector.h"


Vector::Vector(int n) {
	n = (n < MINIMUM) ? MINIMUM : n;
	_elements = new int[n];
	_capacity = n;
	_size = 0;
	_resizeFactor = n;
}

Vector::~Vector() {
	delete[] _elements;
}

#pragma region Getters
int Vector::size() const {
	return _size;
}
int Vector::capacity() const {
	return _capacity;
}
int Vector::resizeFactor() const {
	return _resizeFactor;
}
bool Vector::empty() const {
	return (_size == 0);
}
#pragma endregion

void Vector::push_back(const int& val) {
	if (_size == _capacity)
	{
		reserve(_capacity + 1);
	}
	_elements[_size] = val;
	_size++;
}

int Vector::pop_back() {
	if (_size > 0)
	{
		_size--;
		return _elements[_size];
	}
	else // Isnt really required cause return in the previous block.
	{
		std::cout << "error: pop from empty vector" << std::endl;
		return EMPTY;
	}
}

void Vector::reserve(const int n) {
	int newSize = _capacity;
	int* newVector = nullptr;
	if (_capacity < n)
	{
		do
		{
			newSize += _resizeFactor;
		} while (newSize < n);

		newVector = new int[newSize];
		for (int i = 0; i < _size; i++)
		{
			newVector[i] = _elements[i];
		}
		delete[] _elements;
		_elements = newVector;
		_capacity = newSize;
	}
}

void Vector::resize(int n) {
	int* newVector = nullptr;
	if (n < _capacity)
	{
		newVector = new int[n];
		for (int i = 0; i < n; i++)
		{
			newVector[i] = _elements[i];
		}
		delete[] _elements;
		_elements = newVector;
		_capacity = n;
	}
	else if (n > _capacity)
	{
		reserve(n);
	}
}

void Vector::assign(int val) {
	for (int i = 0; i < _capacity; i++)
	{
		_elements[i] = val;
	}
	_size = _capacity;
}

void Vector::resize(int n, const int& val) {
	int oldSize = _size;
	if (_capacity > n)
	{
		resize(n);
	}
	else if (_capacity < n)
	{
		resize(n);
		_size = n;
		for (int i = oldSize; i < _size; i++)
		{
			_elements[i] = val;
		}
	}
}

Vector::Vector(const Vector& other) {
	// Because we are in the same type of class, we can directly access the fields and don't need to use getters.
	_size = other._size;
	_capacity = other._capacity;
	_resizeFactor = other._resizeFactor;
	_elements = new int[_capacity];
	for (int i = 0; i < _size; i++)
	{
		_elements[i] = other._elements[i];
	}
}

Vector& Vector::operator=(const Vector& other) {
	while (!this->empty()) // cleans the vector
	{
		this->pop_back();
	}
	for (int i = 0; i < other._size; i++)
	{
		this->push_back(other[i]);
	}

	return *this;
}

int& Vector::operator[](int n) const {
	if (n > _size)
	{
		std::cout << "error: accessing a member out of the Vector" << std::endl;
		n = 0;
	}
	return _elements[n];
}

#pragma region Bonuses 

Vector Vector::operator+(const Vector& other) const {
	int vectorLength = (this->_size > other._size) ? this->_size : other._size;
	Vector newVector(vectorLength);
	
	for (int i = 0; i < vectorLength; i++)
	{
		newVector.push_back(((this->_size <= i) ? 0 : (*this)[i]) + ((other._size <= i) ? 0 : other[i])); // Adds the elements from the same index, if the index doesn't exist in the vector its taken as a 0/
	}
	return newVector;
}

Vector Vector::operator-(const Vector& other) const {
	int vectorLength = (this->_size > other._size) ? this->_size : other._size;
	Vector newVector(vectorLength);

	for (int i = 0; i < vectorLength; i++)
	{
		newVector.push_back(((this->_size <= i) ? 0 : (*this)[i]) - ((other._size <= i) ? 0 : other[i])); // Adds the elements from the same index, if the index doesn't exist in the vector its taken as a 0/
	}
	return newVector;
}

void Vector::operator-=(Vector& other) {
	*this = *this - other;
}

void Vector::operator+=(Vector& other) {
	*this = *this + other;
}

std::ostream& operator<<(std::ostream& os, const Vector& v) {
	os << "Vector Info:\nCapacity is " << v._capacity << "\nSize is " << v._size << "\nData is ";
	for (int i = 0; i < v._size; i++)
	{
		os << v[i] << ", ";
	}
	os << std::endl;
	return os;
}

#pragma endregion

#pragma region Bonus bonuses

Vector Vector::operator*(const Vector& other) const {
	int vectorLength = (this->_size > other._size) ? this->_size : other._size;
	Vector newVector(vectorLength);

	for (int i = 0; i < vectorLength; i++)
	{
		newVector.push_back(((this->_size <= i) ? 0 : (*this)[i]) * ((other._size <= i) ? 0 : other[i])); // Adds the elements from the same index, if the index doesn't exist in the vector its taken as a 0/
	}
	return newVector;
}

Vector Vector::operator/(const Vector& other) const {
	int vectorLength = (this->_size > other._size) ? this->_size : other._size;
	Vector newVector(vectorLength);

	for (int i = 0; i < vectorLength; i++)
	{
		newVector.push_back(((this->_size <= i) ? 0 : (*this)[i]) / ((other._size <= i) ? 0 : other[i])); // Adds the elements from the same index, if the index doesn't exist in the vector its taken as a 0/
	}
	return newVector;
}

Vector Vector::operator+(const int other) const {
	int vectorLength = this->_size;
	Vector newVector(vectorLength);

	for (int i = 0; i < vectorLength; i++)
	{
		newVector.push_back((*this)[i] + other);
	}
	return newVector;
}
#pragma endregion
